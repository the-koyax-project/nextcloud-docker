ARG NC_VERSION
FROM nextcloud:${NC_VERSION}

RUN set -ex; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ffmpeg \
        libmagickcore-6.q16-6-extra \
        procps \
        supervisor \
        tesseract-ocr \
    ; \
    rm -rf /var/lib/apt/lists/*

RUN set -ex; \
    \
    savedAptMark="$(apt-mark showmanual)"; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        libbz2-dev \
        libc-client-dev \
        libkrb5-dev \
#        libsmbclient-dev \     #still no smb
    ; \
    \
    docker-php-ext-configure imap --with-kerberos --with-imap-ssl; \
    docker-php-ext-install \
        bz2 \
        imap \
    ; \
#    pecl install smbclient; \                  #even more SMB
#    docker-php-ext-enable smbclient; \         #just keep it out...
    \
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark; \
    ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
        | awk '/=>/ { print $3 }' \
        | sort -u \
        | xargs -r dpkg-query -S \
        | cut -d: -f1 \
        | sort -u \
        | xargs -rt apt-mark manual; \
    \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p \
    /var/log/supervisord \
    /var/run/supervisord \
;

COPY supervisord.conf /

#Eigene PHP Einstellungen
RUN { \
        echo '[www]'; \
        echo 'pm=dynamic'; \
        echo 'pm.max_children=345'; \
        echo 'pm.start_servers=86'; \
        echo 'pm.min_spare_servers=86'; \
        echo 'pm.max_spare_servers=258'; \
        echo 'opcache.memory_consumption=128'; \
    } >> /usr/local/etc/php/conf.d/zz-docker.conf;

# Apache unlimmited Upload Size
RUN echo "LimitRequestBody 0" >> /etc/apache2/apache2.conf

CMD ["/usr/bin/supervisord", "-c", "/supervisord.conf"]
